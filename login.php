<?php
session_start();

require_once "db.php";
require_once "function.php";
$email= "";
$password= "";
$blankchecked= array();
$display_error= array();
$error_message=array('login_error'=>"");

if(isset($_POST['btn_submit'])){

    $val= new Validation();
    $blankchecked= $val->blank_check($_POST);

    if(empty($blankchecked)){
        $email= $_POST['email'];
        $password= $_POST['password'];
        $error_message=array();
        $access= new DB;

        /***** 対策前のコード *****/
        $db= $access ->accessDB();
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql ="SELECT * FROM `admins` WHERE email = '$email'";
        var_dump($sql);exit();  // インジェクションされたSQLのデバッグ

        $stmt = $db->query($sql);
        // $stmt = $db->prepare($sql);
        $stmt ->execute();
        $admin_data= $stmt->fetchAll(PDO::FETCH_ASSOC);
        var_dump($admin_data);exit();


        
        /***** 対策後のコード *****/
        /*
        $db= $access ->accessDB();
        // エミュレータをオフにする(複文の禁止、静的プレースホルダを使用する)
        $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        // エラーコードを設定、エラーをスロー
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        // プレースホルダを使用して値をバインドする
        $sql ="SELECT * FROM `admins` WHERE email = :email";
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':email', $email, PDO::PARAM_INT);
        $stmt->execute();
        $admin_data= $stmt->fetchAll(PDO::FETCH_ASSOC);
        var_dump($admin_data);exit();
        */

    }else{
        $email= $_POST['email'];
        $password= $_POST['password'];
    }
}
$vali= new Validation();
$display_error= $vali->display_Error($blankchecked);
?>





<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>ログイン画面</title>
    </head>
    <body>
    <h1>ログイン画面</h1>
        <form action="" method="POST">
            <p>メールアドレスを入力して下さい</p>
            <input type="text" name="email" placeholder="メールアドレス" value="<?php echo $email;?>">
            <p class="errorMessage"><?php echo $display_error['emailError']; ?></p>

            <p>パスワードを入力して下さい</p>
            <input type="text" name="password" placeholder="パスワード" value="<?php echo $password;?>">
            <p class="errorMessage"><?php echo $display_error['passwordError']; ?></p><br><br>
            <input type="submit" name="btn_submit" value="ログインする"> 
        </form>

        <p>アカウントを登録する</p>
        <form action="">
            <input type="submit" value="登録画面へ">
        </form>
    </body>
</html>

<style>
	.errorMessage{
		color: orange
	}
</style>