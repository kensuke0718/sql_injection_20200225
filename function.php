<?php

class Validation{
    public function blank_check($input_data){
        $error = array();
        
        if(empty($input_data['email'])){
            $error['noEmail']= "メールアドレスを入力して下さい";
        }

        if(empty($input_data['password'])){
            $error['noPassword']= "パスワードを入力して下さい";
        }
        return $error;
    }

    public function display_Error($checked_data){
        $error_message =array();
        
        if(isset($_POST['btn_submit'])){

            if(isset($checked_data['noEmail'])){ 
                $error_message['emailError']= $checked_data['noEmail'];
            }else{
                $error_message['emailError']= "";
            }

            if(isset($checked_data['noPassword'])){ 
                $error_message['passwordError']= $checked_data['noPassword'];
            }else{
                $error_message['passwordError']= "";
            }
            return $error_message;
        }
    }
}


?>