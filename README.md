# 作業手順

1. テスト用 DB の作成
2. サンプルデータ登録
3. php ファイルから接続確認
4. インジェクション攻撃実施
5. インジェクション対策実装
6. 対策ができたか検証

---

## 1. テスト用の DB 作成

```sql
CREATE DATABASE test;
```

### admin 　テーブル作成 sql

```sql
CREATE TABLE `test`.`admin` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , `email` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , `password` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
```

---

## 2. サンプルデータ登録

```sql
INSERT INTO `admins` (`id`, `name`, `email`, `password`) VALUES (NULL, 'test', 'test@gmail.com', 'pass'), (NULL, 'test2', 'test2@gmail.com', 'pass2')
```

---

## 3. php ファイルから接続確認

メール入力欄にインジェクションする SQL を記入。

パスワード入力欄には適当に値を記入し、「ログインする」ボタンを押す

### 「接続成功」が表示されたら OK

---

## 4. インジェクション攻撃実施

### 1. 会員データ全取得する

```sql
' OR 'A' = 'A
```

### 2. 会員データを削除する

```sql
'; DELETE FROM admins WHERE 'A' = 'A
```

### 3. その他例

※　'; 以降に実施したい SQL を記入すれば実行可能

- 全てのユーザの名前を hoge にする

```sql
'; UPDATE `admins` SET `name` = 'hoge' WHERE 'A' = 'A
```

- admin テーブルを削除する

```sql
DROP TABLE `admin`
```

- testDB を削除する

```sql
DROP DATABASE `test`
```

---

## 5. インジェクション対策実装

```php
$db= $access ->accessDB();
// エミュレータをオフにする(複文の禁止、静的プレースホルダを使用する)
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
// エラーコードを設定、エラーをスロー
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
// プレースホルダを使用して値をバインドする
$sql ="SELECT * FROM `admins` WHERE email = :email";
$stmt = $db->prepare($sql);
$stmt->bindValue(':email', $email, PDO::PARAM_INT);
$stmt->execute();
$admin_data= $stmt->fetchAll(PDO::FETCH_ASSOC);
```

---

## 6. 対策ができたか検証

```php
// 「' OR 'A' = 'A」を入力して取得したデータをデバッグ
// 何も取得できていなければ成功
var_dump($admin_data);exit();
```

ターミナルでエラーログの確認

エラーログの場所(XAMPP の場合)

XAMPP/xamppfiles/var/mysql/自分の PC 名前.local.err

```sh
$ sudo tail -200f ログファイル名
```

---

## 参考 URL

- https://qiita.com/kurodenwa/items/8807e79515c0e2b4dad9
- https://qiita.com/stk2k/items/c46cc921a4f7b6e4bab2
- https://qiita.com/mpyw/items/b00b72c5c95aac573b71#%E3%82%A8%E3%83%9F%E3%83%A5%E3%83%AC%E3%83%BC%E3%82%B7%E3%83%A7%E3%83%B3%E3%81%AB%E9%96%A2%E3%81%99%E3%82%8B%E3%81%BE%E3%81%A8%E3%82%81
- https://blog.tokumaru.org/2013/12/pdo-and-mysql-allow-multiple-statements.html
